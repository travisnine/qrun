#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

// TODO: REMOVE ALL debug_print LINES
#define debug_print printf

#define append(mem,memsize,val) mem = realloc(mem,(++memsize) * sizeof(val));\
                                if (mem == 0)\
                                    return 1;\
                                mem[memsize-1] = val;

void freestrarr(char** array, size_t size) {
    for (size_t x = 0; x < size; x++) {
        free(array[x]);
        array[x] = 0;
    }
    free(array);
    array = 0;
}

int main(const int argc, const char** argv) {
    // argument parsing
    if (argc < 2) {
        printf("USAGE: %s program {}\n",argv[0]);
        return 1;
    }
    const char* myProg = argv[1];
    debug_print("program to run: %s\n",myProg);
    char** myArgs = 0;
    int myArgsSize = 0;
    int* myArgIdx = 0;
    int myArgIdxSize = 0;
    append(myArgs,myArgsSize,strdup(myProg));
    if (argc > 2) {
        for (int x = 2; x < argc; x++) {
            int curIdx = x - 1;
            if (strcmp(argv[x],"{}") == 0) {
                append(myArgIdx,myArgIdxSize,curIdx);
                debug_print("Found replacement {} at location %i\n",myArgIdx[myArgIdxSize-1]);
            }
            append(myArgs,myArgsSize,strdup(argv[x]));
            debug_print("Added Arg %s\n",myArgs[myArgsSize-1]);
        }
    }
    append(myArgs,myArgsSize,(char *)0);

    // setup pipe to pass messages
    int p[2] = {0,0};
    if (pipe(p) == -1)
        return 1;
    int pr = p[0];
    int pw = p[1];

    // spawn queue runner
    pid_t qChild = -1;
    qChild = fork();
    if (qChild == -1)
        return 1;
    if (qChild == 0) {
        // qrunner qChild
        close(pw);
        FILE* piper = 0;
        piper = fdopen(pr,"r");
        if (piper == 0)
            return 1;

        char* in = 0;
        size_t ins = 0;
        size_t inc = 0;
        for (;;) {
            inc = getline(&in,&ins,piper);
            if (inc == -1) {
                freestrarr(myArgs,myArgsSize);
                free(myArgIdx);
                free(in);
                fclose(piper);
                puts("QUEUE FINISHED");
                wait(0);
                return 0;
            }
            if (inc > 0) {
                in[--inc] = '\0'; // remove trailing newline
                // replacement
                for (int x = 0; x < myArgIdxSize; x++) {
                    free(myArgs[myArgIdx[x]]);
                    myArgs[myArgIdx[x]] = strdup(in);
                }
                // print what we about to do
                printf("RUNNING: [ ");
                for (int x = 0; x < (myArgsSize - 1); x++) {
                    printf("'%s' ",myArgs[x]);
                }
                puts("]");
                // spawn child
                if (vfork() == 0) {
                       execvp(myProg,myArgs);
                       return 0;
                }
                wait(0);
            }
        }
        return 0;
    }
    
    // stdin loop
    close(pr);
    char* in = 0;
    size_t ins = 0;
    size_t inc = 0;
    for (;;) {
        inc = getline(&in,&ins,stdin);
        if (inc == -1) {
            freestrarr(myArgs,myArgsSize);
            free(myArgIdx);
            free(in);
            write(pw,0,0);
            close(pw);
            puts("EOF Reached, waiting for queue to finish");
            wait(0);
            return 0;
        }
        if (inc > 1) {
            write(pw,in,inc);
            printf("ADDED: (%i) %s",inc,in);
        }
    }
}
