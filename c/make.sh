#!/bin/bash

MAKECMD='musl-gcc'
#MAKECMD='gcc'
opts='-static -O2'
TARGET='
-o qrun
qrun.c
'
$MAKECMD $opts $TARGET
strip qrun
