module main

import os

const buffer_size = 1024 * 1024

struct QRUN_DATA {
	mut:
		args []string
		args_idx []int
}

fn queue_runner(mut d QRUN_DATA, queue chan string) {
	for {
			q_item := <-queue or { break }
			for idx in d.args_idx {
				d.args[idx] = q_item
			}
			eprintln("Running: ${d.args}")
			cmd := d.args[0]
			args := d.args[1..]
			if os.fork() == 0 {
				os.execvp(cmd, args) or { 
					eprintln("ERROR: unable to execvp(${cmd}, ${args})")
					exit(1)
				}
				exit(0)
			}
			C.wait(0)
	}
}

fn main() {
	if os.args.len < 3 {
		eprintln("Usage: ${os.args[0]} program {}")
		exit(1)
	}
	// parse data
	mut d := QRUN_DATA{}
	queue := chan string {cap: buffer_size}
	for argidx in 1..os.args.len {
		curval := os.args[argidx]
			d.args << curval
			if curval == "{}" {
				d.args_idx << argidx - 1
			}
	}
	if d.args_idx.len < 1 {
		eprintln("WARNING: You haven't included any {} in the argument list")
	}
	child := spawn queue_runner(mut &d, queue)
	for {
		inputstr := os.input("")
		if inputstr == "<EOF>" {
			break
		}
		queue <- inputstr
	}
	queue.close()
	eprintln("EOF reached, waiting for q to empty")
	child.wait()
	eprintln("DONE")
	exit(0)
}
