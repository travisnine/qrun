#!/bin/bash

# parse params
declare -i ARGIDX=0
declare -a ARG_REPLACE_IDX=()
declare -a ARGS=()
for ARG in "$@"
do
	if [[ "$ARG" == "{}" ]]
	then
		ARG_REPLACE_IDX+=("$ARGIDX")
	fi
	ARGS+=("$ARG")
	ARGIDX+=1
done

if [[ ! "${ARG_REPLACE_IDX[*]}" ]]
then
	echo 'WARNING: You havent included any {} in the arugment list'
fi

echo "Queue will run: bash -c \"${ARGS[*]}\""

ee () { echo "$*" >&2 ; }

{
# stdin loop
while read QRUNCMDIN
do
	for IDX in "${ARG_REPLACE_IDX[@]}"
	do
		ARGS[$IDX]="'$QRUNCMDIN'"
	done
	ee "ADDED: bash -c \"${ARGS[*]}\""
	echo "${ARGS[*]}"
done
ee 'GOT EOF. WAITING FOR QUEUE TO COMPLETE'
} | {
# qrunner
while read QRUNCMD
do
	ee "RUNNING: bash -c \"$QRUNCMD\""
	bash -c "$QRUNCMD"
	ee "DONE: bash -c \"$QRUNCMD\""
done
ee 'QUEUE EXIT'
}

