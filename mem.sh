#!/bin/bash

echo1000 () {
	for x in {1..1000}
	do
		echo $RANDOM
	done
}

out () {
	echo "$*" | tee -a mem.sh-results.txt
}

out "$(date)"
out "$(pmap -X $$ | head -n2 | tail -n1)"

for prog in go/qrun bash/qrun.sh python/qrun.py rust/target/release/qrun c/qrun v/qrun
do
	echo1000 | "$prog" sleep 10 &
	CURPID=$!
	sleep 2
	out "    $prog"
	#out "        $(cat /proc/$CURPID/statm)"
	out "$(pmap -X $CURPID | tail -n 1)"
	sleep 1
	kill -15 $CURPID
done


